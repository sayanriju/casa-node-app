const mongoose = require("mongoose")

const ContactSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true
  },

  name: {
    first: String,
    middle: String,
    last: String
  }
})

ContactSchema.set("toJSON", { virtuals: true })
ContactSchema.set("toJSON", { virtuals: true })

module.exports = mongoose.model("Contact", ContactSchema)
