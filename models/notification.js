const mongoose = require("mongoose")

const NotificationSchema = new mongoose.Schema({
  what: { type: String, required: true },
  when: { type: Date, default: Date.now },
  for: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  unread: { type: Boolean, default: true }
})

NotificationSchema.set("toJSON", { virtuals: true })
NotificationSchema.set("toJSON", { virtuals: true })

module.exports = mongoose.model("Notification", NotificationSchema)
