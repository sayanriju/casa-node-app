const mongoose = require("mongoose")

const CouponSchema = new mongoose.Schema({
  code: {
    type: String,
    required: true,
    uppercase: true
  },

  discountPercentage: {
    type: Number,
    required: true,
    max: 100,
    min: 0,
    default: 0
  },

  createdAt: { type: Date, default: Date.now },
  expiredAt: { type: Date, default: null }, // NEVER expire by default

  _createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
})

CouponSchema.set("toJSON", { virtuals: true })
CouponSchema.set("toJSON", { virtuals: true })

module.exports = mongoose.model("Coupon", CouponSchema)
