const axios = require("axios")
const config = require("./config")[process.env.NODE_ENV || "development"]

module.exports = {
  send(playerIds, msg) {
    if (playerIds.length === 0) {
      return "NO DEVICES"
    }
    axios.post(config.onesignal.createNotificationUrl, {
      app_id: config.onesignal.appId,
      contents: { en: msg },
      include_player_ids: playerIds
    }, {
      headers: {
        "Content-Type": "application/json; charset=utf-8",
        Authorization: `Basic ${config.onesignal.restApiKey}`
      }
    })
    .then((response) => {
      console.log("OneSignal Success: ", response.data)
    })
    .catch((error) => {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log("OneSignal Error: ", error.response.data)
        // reject(error.response.status);
        // reject(error.response.headers);
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log("OneSignal Error: ", error.request)
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log("OneSignal Error: ", new Error(error.message))
      }
      console.log("OneSignal Error: ", error.config)
    })
    return "OK" // no waiting!
  }
}
