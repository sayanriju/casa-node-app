const User = require("../../models/user")

const jwt = require("jsonwebtoken")
const config = require("../../config")[process.env.NODE_ENV || "development"]

module.exports = {

  post: (req, res) => {
    const handle = req.body.loginEmail
    const pass = req.body.loginPassword
    if (!handle || !pass) {
      res.json({
        error: true,
        message: "Empty Credentials"
      })
      return
    }

    User
    .findOne({ email: handle })
    .exec()
    .then((user) => {
      if (!user) {
        res.json({
          error: true,
          message: "User Not Found"
        })
        return
      }
      if (!user.isActive) {
        res.json({
          error: true,
          message: "User Inactive"
        })
        return
      }

      user.comparePassword(pass, (err1, isMatch) => {
        if (isMatch && !err1) {
          const payload = {
            id: user._id,
            fullName: user.name.full,
            email: user.email,
          }
          const token = jwt.sign(payload, config.secret, {
            expiresIn: 3600 * 24 * 30
          })
          res.json({
            error: false,
            message: "Logged in successfully",
            token
          })
        } else {
          res.json({
            error: true,
            message: "Password mismatch"
          })
        }
      })
    })
    .catch(err => res.json({ error: true, reason: err, message: "Network Error" }))
  },

}
