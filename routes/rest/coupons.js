const Coupon = require("../../models/coupon")

module.exports = {
  validate(req, res) {
    const code = req.body.code.toUpperCase()
    Coupon
    .findOne({ code })
    .exec()
    .then((coupon) => {
      if (coupon === null) {
        return res.json({ error: true, reason: "Invalid Coupon Code" })
      }
      return res.json({ error: false, coupon })
    })
    .catch(e => ({ error: true, reason: e.message }))
  }
}