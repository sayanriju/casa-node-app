const User = require("../../models/user")

module.exports = {
  get: (req, res) => {
    User
    .findOne({ _id: req.user.id })
    .select("-password")
    .exec()
    .then(userdata => res.json({ error: false, result: userdata }))
    .catch(err => res.json({ error: true, reason: err.message }))
  },

  post(req, res) {
    const data = req.body
    if (data.userType === "Admin") data.userType = "Member" // dont allow people to register themselves as admin
    // console.log('*******xxxxxxxx*****',data)
    data.isActive = false // app users are actiavted by admin after verification
    const user = new User(data)
    return user.save()
    .then(savedUser => res.json({ error: false, saved: savedUser }))
    .catch((err) => {
      console.log(err)
      return res.json({
        error: true,
        reason: err,
        message: "Details not saved"
      })
    })
  },
}
