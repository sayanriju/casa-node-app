const User = require("../../models/user")
const jwt = require("jsonwebtoken")
const config = require("../../config")[process.env.NODE_ENV || "development"]
const TinyURL = require("tinyurl")

module.exports = {
  postsendchangepassmail: (req, res) => {
    const payload = {
      handle: req.body.handle, // the email for whom to change pass
    }
    User.count({ email: req.body.handle, isActive: true }, (err, count) => {
      if (err || !count) {
        return res.json({ complete: false, error: true, message: `No active user with email "${req.body.handle}" exists in our Database!` })
      }
      const token = jwt.sign(payload, config.secret, { expiresIn: "1h" })

      TinyURL.shorten(config.siteUrl + "/api/v1/changepassword/" + token, (shorturl) => {
        const locals = {
          to: req.body.handle,
          // to: 's26c.sayan@gmail.com', // for testing
          subject: "Password Change Request for Casa",
          handle: req.body.handle,
          changePassURL: shorturl
        }
        res.mailer.send("emails/forgotpassword", locals, (error) => {
          if (!error) {
            console.log("Password email sent successfully to %s !", locals.handle)
            //  res.send('done!');
          } else {
            console.log("error: %o", error)
            // res.send({error: true})
          }
        })
      })

      return res.json({ complete: true, error: false }) // Although we don't actually wait for email sending to complete!
    })
  },

  getChangePass: (req, res) => {
    const token = req.params.token
    const decoded = jwt.decode(token) // NOT verified!!
    // res.json({ token: token, handle: decoded.handle });
    res.render("changepassword", { token: token, handle: decoded.handle })
  },

  postChangePass: (req, res) => {
    const token = req.body.token
    const newpass = req.body.newpass
    jwt.verify(token, config.secret, (error, decoded) => {
      if (error) {
        return res.json({ error: true, message: "Invalid or Expired Token!", reason: error })
      }
      const handle = decoded.handle
      // Now change password in DB
      User
      .findOne({ email: handle })
      .then((user) => {
        user.password = newpass
        return user.save()
      })
      .then(savedUser => res.json({ error: false, saveduser: savedUser }))
      .catch(err => res.json({ error: true, message: "Failed to change password for" + handle, reason: err }))
    })
  }

}
