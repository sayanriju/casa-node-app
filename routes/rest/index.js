const express = require("express")
const router = express.Router()
const config = require("../../config")[process.env.NODE_ENV || "development"]

// const jwt = require("jsonwebtoken")
const expressJwt = require("express-jwt")
const checkJwt = expressJwt({ secret: config.secret }) // the JWT auth check middleware

const users = require("./users")
const login = require("./login")
const notifs = require("./notifications")
const grievance = require("./grievance")
const coupons = require("./coupons")
const changepassword = require("./changepassword")

// Login routes:
router.post("/login", login.post) // unauthenticated

/** Login into Node App */
router.post("/usersignup", users.post)
router.get("/getuserinfo", checkJwt, users.get)

// Change password
router.get("/changepassword/:token", changepassword.getChangePass)
router.post("/changepass", changepassword.postChangePass)
router.post("/sendchangepassmail", changepassword.postsendchangepassmail)

// router.post("/webhook", notifs.webhook)  // Fetch Contacts from Constant Contacts: to be run in the bg

router.post("/notification/device", checkJwt, notifs.addDevice) // for push notifs using onesignal
router.delete("/notification/device/:id", checkJwt, notifs.removeDevice) // for push notifs using onesignal

router.get("/notifications", checkJwt, notifs.get) // get all unread notifs for logged in user [Poll this!]
router.post("/notifications/markread/:id?", checkJwt, notifs.post) // the main (and only) action

router.post("/grievancemail", checkJwt, grievance.post)

// Coupons
router.post("/coupons/validate", coupons.validate)  // unauthenticated! (For public consumption)

module.exports = router
