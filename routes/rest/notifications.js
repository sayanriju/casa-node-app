const Notification = require("../../models/notification")
const User = require("../../models/user")

module.exports = {
  get(req, res) {
    const query = { for: req.user.id, unread: true }
    let limit
    if (req.query.all) {
      delete query.unread
    }
    if (req.query.limit && !isNaN(req.query.limit)) {
      limit = Number(req.query.limit)
    }
    Notification
    .find(query)
    .sort({ when: -1 })
    .limit(limit)
    .exec()
    .then(notifications => res.json({ error: false, notifications }))
    .catch(err => res.json({ error: true, reason: err.message }))
  },

  post(req, res) {
    const query = { for: req.user.id }
    if (req.params.id) {
      query._id = req.params.id
    }
    Notification
    .update(query, { unread: false }, { multi: true })
    .then(() => res.json({ error: false }))
    .catch(err => res.json({ error: true, reason: err.message }))
  },

  //  For push notifs usign Onesignal
  addDevice(req, res) {
    User
    .findOne({ _id: req.user.id })
    .exec()
    .then((user) => {
      if (user === null) {
        return res.json({ error: true, reason: "No Such User" })
      }
      const existingDevices = user.devices || []
      user.devices = [...new Set([...existingDevices, req.body.deviceId])] // prune duplicates, if any
      return user.save()
    })
    .then(user => res.json({ error: false, userId: user._id, devices: user.devices }))
    .catch(err => res.json({ error: true, reason: err.message }))
  },

  removeDevice(req, res) {
    User
    .findOne({ _id: req.user.id })
    .exec()
    .then((user) => {
      if (user === null) {
        return res.json({ error: true, reason: "No Such User" })
      }
      const existingDevices = user.devices || []
      user.devices = existingDevices.filter(d => String(d) !== String(req.params.id))
      return user.save()
    })
    .then(user => res.json({ error: false, userId: user._id, devices: user.devices }))
    .catch(err => res.json({ error: true, reason: err.message }))
  }
}
