const express = require("express")
const router = express.Router()

const users = require("./users")
const constantcontact = require("./constantcontact")
const notifs = require("./notifs")
const coupons = require("./coupons")
const login = require("./login")

// Login routes:
router.get("/login", login.get)
router.get("/forgetpassword", login.forgetpass)
router.post("/login", login.post) // unauthenticated

router.all("/logout", login.logout)

// router.any("/logout", users.logout)

router.use((req, res, next) => {  // Run this check auth middleware for ALL subsequent routes
  if (req.session.user === undefined || !req.session.user.authenticate) {
    return res.redirect("/logout")
  }
  return next()
})
/** Login into Node App */
// router.get("/getuserinfo", users.get)
router.get("/", users.getAll) // alias
router.get("/user", users.getAll)
router.get("/users", users.getAll)
// router.get("/sendemail", users.email)
router.post("/addnewuser", users.post)
router.post("/activedeactiveuser/:id", users.changeStatus)

router.get("/notifs", notifs.get)
router.post("/notifs", notifs.post)

// router.get("/", dashboard.listUsers)
// router.get("/userlist", dashboard.listUsers)  // alias
router.get("/email", constantcontact.get) // show view for composing emails
router.post("/email", constantcontact.post) // send emails
router.post("/fetchcontacts", constantcontact.fetchContacts) // fetch from Constant Contact (to be run in bg)

// Coupons
router.post("/coupons", coupons.post)
router.get("/coupons", coupons.get)
router.delete("/coupons/:id", coupons.delete)

module.exports = router
