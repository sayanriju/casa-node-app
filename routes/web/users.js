const User = require("../../models/user")
const randomstring = require("randomstring")


module.exports = {
  get: (req, res) => {
    // console.log("*99988***", req.session.user.id)
    User
    .findOne({ _id: req.session.user.id })
    .select("-password")
    .exec()
    .then(userdata => res.render("userdata", { error: false, result: userdata }))
    .catch(err => res.json({ error: true, reason: err.message }))
  },

  email: (req, res) => {
    res.render("email")
  },

  getAll: (req, res) => {
    User
    .find()
    .select("-password")
    .sort("-createdAt")
    .exec()
    .then((users) => {
      res.render("users", { error: false, users })
    })
    .catch(err => res.json({ error: true, reason: err.message }))
  },

  post(req, res) {
    const data = req.body
    // console.log('*******xxxxxxxx*****',data)
    data.password = randomstring.generate(6)
    const user = new User(data)
    return user.save()
    .then((saveduser) => {
      const locals = {
        to: saveduser.email,
        subject: "Welcome to Casa de Amparo!",
        userName: saveduser.name.full,
        emailId: saveduser.email,
        password: data.password,
        type: saveduser.userType
      }
      res.mailer.send("emails/send_user_welcomemail", locals, (error) => {
        if (!error) {
          console.log("Email sent successfully to %s !")
          return res.json({ error: false, saved: saveduser })
        }
        console.log("error: %o", error)
        return res.json({ error: true, message: "email not send" })
      })
    })
    .catch((err) => {
      console.log(err)
      return res.json({
        error: true,
        reason: err,
        message: "Details not saved"
      })
    })
  },

  changeStatus: (req, res) => {
    User
    .findOne({ _id: req.params.id })
    .exec()
    .then((user) => {
      if (user.isActive) {
        user.isActive = false
      } else {
        user.isActive = true
      }
      return user.save()
    })
    .then((savedUser) => {
      // send email on app user activation (BUT NO WAITING!!):
      if (savedUser.isActive === true && savedUser.userType !== "Admin") {
        const locals = {
          to: savedUser.email,
          subject: "Casa de Amparo: Account Activated",
          userName: savedUser.name.full,
        }
        res.mailer.send("emails/user_activated", locals, (error) => {
          if (!error) {
            console.log("Email sent successfully to %s !")
          }
          console.log("email sending error: %o", error)
        })
      }
      return res.json({ error: false, msg: "Status changed successfully", user: savedUser })
    })
    .catch(err => res.json({ error: true, reason: err.message }))
  },
}
