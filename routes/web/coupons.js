const Coupon = require("../../models/coupon")
const moment = require("moment")

module.exports = {
  get(req, res) {
    Coupon
    .find()
    .sort("-createdAt")
    .exec()
    .then((coupons) => {
      res.render("coupons", { error: false, moment, coupons })
    })
    .catch(err => res.json({ error: true, reason: err.message }))
  },

  post(req, res) {
    const { code, discountPercentage } = req.body
    const _createdBy = req.session.user.id
    Coupon
    .create({ code, discountPercentage, _createdBy })
    .then(newCoupon => res.json({ error: false, newCoupon }))
    .catch(err => res.json({ error: true, reason: err.message }))
  },

  delete(req, res) {
    const idToDelete = req.params.id
    Coupon
    .remove({ _id: idToDelete })  // anybody who's logged in can delete
    // .remove({ _id: idToDelete, _createdBy: req.session.user.id })
    .exec()
    .then(() => res.json({ error: false }))
    .catch(err => res.json({ error: true, reason: err.message }))
  },
}