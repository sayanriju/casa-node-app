const User = require("../../models/user")
const Notification = require("../../models/notification")
const Contact = require("../../models/contact")
const axios = require("axios")

const config = require("../../config")[process.env.NODE_ENV || "development"]

const onesignal = require("../../onesignal")

module.exports = {

  listUsers(req, res) {
    User
    .find()
    .exec()
    .then(users => res.render("userlist", { error: false, users }))
    .catch(err => res.json({ error: true, reason: err }))
  },

  async fetchContacts(req, res) {
    let flag = true
    let counter = 0
    const maxCounter = 10
    let nextLink = `${config.constantContact.baseURL}/contacts/?api_key=${config.constantContact.apiKey}&limit=50`
    const contacts = []
    while (flag) {
      if (counter > maxCounter) break
      try {
        console.log("1111", nextLink)
        const response = await axios.get( // eslint-disable-line no-await-in-loop
          nextLink,
          { headers: { Authorization: `Bearer ${config.constantContact.accessToken}` } }
        )
        contacts.push(...response.data.results.map(item => ({
          email: (item.email_addresses.length) ? item.email_addresses[0].email_address : "",
          name: {
            first: item.first_name,
            middle: item.middle_name,
            last: item.last_name
          }
        })))
        if (response.data.meta && response.data.meta.pagination && response.data.meta.pagination.next_link) {
          nextLink = `${config.constantContact.baseURL.replace("/v2", "")}${response.data.meta.pagination.next_link}&api_key=${config.constantContact.apiKey}`
          console.log("NEXT LINK: ", response.data.meta.pagination.next_link)
        } else {
          flag = false
        }
      } catch (e) {
        flag = false
        console.log("ERROR: ", e.message)
      }
      counter += 1
    }
    console.log(`Contacts cnt: ${contacts.length}`)
    Contact
    .insertMany(contacts)
    .then(() => res.json({ error: false }))
    .catch(err => res.json({ error: true, reason: err.message }))
  },

  get(req, res) {
    let lists
    // Fetch ALL email lists from CC
    axios.get(
      `${config.constantContact.baseURL}/lists/?api_key=${config.constantContact.apiKey}`,
      { headers: { Authorization: `Bearer ${config.constantContact.accessToken}` } }
    )
    .then((response) => {
      lists = response.data
      /**
       * [{
          "id": "1552191403",
          "name": "Sweepstakes",
          "status": "HIDDEN",
          "created_date": "2014-08-06T22:00:09.000Z",
          "modified_date": "2014-08-22T18:09:30.000Z",
          "contact_count": 9
        } ... ]
       */
      // Fetch ALL Campaigns
      return axios.get( // eslint-disable-line no-await-in-loop
        `${config.constantContact.baseURL}/emailmarketing/campaigns/?status=DRAFT&api_key=${config.constantContact.apiKey}`,
        { headers: { Authorization: `Bearer ${config.constantContact.accessToken}` } }
      )
    })
    .then((response) => {
      const campaigns = response.data.results
      /**
       * [{
       "id": "1120087983842",
       "name": "cxrehftab",
       "status": "DRAFT",
       "modified_date": "2017-09-28T21:17:22.000Z"
      } ... ]
      */
      // res.json({ lists, campaigns })
      return res.render("email", { lists, campaigns })
    })
    .catch(err => res.json({ error: true, reason: err.message }))
  },

  post(req, res) {
    const lists = req.body.lists  // Constant Contact ID(s)
    const campaign = req.body.campaign  // Constant Contact ID(s)
    const sendNotif = (req.body.sendnotif === "yes")
    const notifSubject = req.body.subject  // used only as the notification text, since subject of campaign is preset

    const notifiableUsers = []
    let notifiableDevices = []

    if (lists.length === 0 || !campaign) {
      res.json({ error: true, reason: "Missing Mandatory Fields!" })
    } else {
      // Get signed up users
      User
      .find()
      .lean()
      .exec()
      .then((allUsers) => { // eslint-disable-line arrow-body-style
        if (!sendNotif) {
          return Promise.resolve()
        }
        notifiableDevices = [...new Set(notifiableUsers
          .map(u => u.devices)
          .reduce((acc, cur) => [...acc, ...cur], []))]
        return (async function () {  // async IIFE, so returns a promise!
          for (const user of allUsers) {  // eslint-disable-line no-restricted-syntax
            // Fetch all Lists for which this user email is member of [Constant Contact]
            try {
              const response = await axios.get( // eslint-disable-line no-await-in-loop
                `${config.constantContact.baseURL}/contacts?email=${user.email}&api_key=${config.constantContact.apiKey}`,
                { headers: { Authorization: `Bearer ${config.constantContact.accessToken}` } }
              )
              const commonList = response.data.results[0].lists.map(l => l.id).find(lid => lists.includes(lid))
              if (commonList !== undefined) notifiableUsers.push(user)
            } catch (e) {
              console.log("Error fetching lists for a user: ", e.message)
            }
          }
        }())
      })
      .then(() => { // eslint-disable-line arrow-body-style
        // First, fetch all details about selected Campaign, which will be edited later [Constant Contact]
        return axios.get(
          `${config.constantContact.baseURL}/emailmarketing/campaigns/${campaign}?api_key=${config.constantContact.apiKey}`,
          { headers: { Authorization: `Bearer ${config.constantContact.accessToken}` } }
        )
      })
      .then((response) => { // eslint-disable-line arrow-body-style
        console.log("11111", response.data)
        // Selected Campaign data (required fields for PUT):
        const { name, subject, from_name, from_email, reply_to_email } = response.data
        // Add List(s) to Campaign [Constant Contact]
        return axios.put(
          `${config.constantContact.baseURL}/emailmarketing/campaigns/${campaign}?api_key=${config.constantContact.apiKey}`,
          { sent_to_contact_lists: lists.map(l => ({ id: l })), name, subject, from_name, from_email, reply_to_email },
          { headers: { Authorization: `Bearer ${config.constantContact.accessToken}` } }
        )
      })
      .then((response) => {
        console.log("2222", response.data)
        // Schedule the Campaign to be sent *now* to List(s) [Constant Contact]
        return axios.post(
          `${config.constantContact.baseURL}/emailmarketing/campaigns/${campaign}/schedules?api_key=${config.constantContact.apiKey}`,
          { },
          { headers: { Authorization: `Bearer ${config.constantContact.accessToken}` } }
        )
      })
      .then((response) => {
        console.log("3333", notifiableUsers, response.data)
        // Create Notifications
        if (sendNotif) {
          const now = Date.now()
          const notifs = notifiableUsers
            .map(u => ({ for: u._id, when: now, what: notifSubject }))

          // send Push Notifs using OneSignal to all notifiable users using their device / player ids ...
          onesignal.send(notifiableDevices, notifSubject) // ... but no waiting!

          return Notification.insertMany(notifs)
        }
        return Promise.resolve()
      })
      .then(() => res.json({ error: false }))
      .catch(err => res.json({ error: true, reason: err.message }))
    }
  }

}
