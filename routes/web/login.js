const User = require("../../models/user")

module.exports = {
  get: (req, res) => {
    res.render("casa_Login")
  },

  forgetpass: (req, res) => {
    res.render("casa_Forgot-Password")
  },

  post: (req, res) => {
    const handle = req.body.loginEmail
    const pass = req.body.loginPassword
    if (!handle || !pass) {
      res.json({
        error: true,
        message: "Empty Credentials"
      })
      return
    }

    User
    .findOne({ email: handle, userType: "Admin" })  // Allow only admins to login into this CP
    .exec()
    .then((user) => {
      if (!user) {
        res.json({
          error: true,
          message: "User Not Found"
        })
        return
      }
      if (!user.isActive) {
        res.json({
          error: true,
          message: "User Inactive"
        })
        return
      }

      user.comparePassword(pass, (err1, isMatch) => {
        if (isMatch && !err1) {
          const payload = {
            id: user._id,
            fullName: user.name.full,
            email: user.email,
            authenticate: true
          }
          req.session.user = payload
          res.json({
            error: false,
            message: "Logged in successfully"
          })
        } else {
          res.json({
            error: true,
            message: "Password mismatch"
          })
        }
      })
    })
    .catch(err => res.json({ error: true, reason: err, message: "Network Error" }))
  },

  logout: (req, res) => {
    req.session.destroy()
    res.redirect("/login")
  },
}
