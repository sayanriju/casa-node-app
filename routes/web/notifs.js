const User = require("../../models/user")
const Notification = require("../../models/notification")

const config = require("../../config")[process.env.NODE_ENV || "development"]

const onesignal = require("../../onesignal")

module.exports = {

  get(req, res) {
    User
    .find()
    .exec()
    .then(users => res.render("notifs", { error: false, users }))
    .catch(err => res.json({ error: true, reason: err }))
  },

  post(req, res) {
    let notifiableDevices = []
    User
    .find()
    .select("_id devices")
    .lean()
    .exec()
    .then((allUsers) => {
      console.log("111", allUsers)
      let notifiableUsers
      if (req.body.all === "no") {
        notifiableUsers = allUsers.filter(u => req.body.users.includes(String(u._id)))
      } else {
        console.log("req.body.all ? ", req.body.all);
        notifiableUsers = allUsers
      }
      console.log("222", notifiableUsers)
      notifiableDevices = notifiableUsers
        .map(u => u.devices)
        .filter(d => d !== undefined)
        .reduce((acc, cur) => [...acc, ...cur], [])
      notifiableDevices = [...new Set(notifiableDevices)] // prune duplicates

      // send Push Notifs using OneSignal to all notifiable users using their device / player ids ...
      console.log("!!!!!!!!!!!!!!!", notifiableDevices)
      onesignal.send(notifiableDevices, req.body.subject) // ... but no waiting!

      const now = Date.now()
      const notifs = notifiableUsers
        .map(u => ({ for: u._id, when: now, what: req.body.subject }))
      return Notification.insertMany(notifs)
    })
    .then(() => res.json({ error: false }))
    .catch(e => res.json({ error: true, reason: e.message }))
  },

}
